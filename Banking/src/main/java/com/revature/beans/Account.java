package com.revature.beans;

import java.text.NumberFormat;
import java.util.Locale;

public class Account {
	private int id;
	private String type;
	private float balance;
	private static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);

	public Account() {

	}

	public Account(int id, String type, float balance) {
		super();
		this.id = id;
		this.type = type;
		this.balance = balance;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		String printType = "";
		if (type.equals("C")) {
			printType = "Checking";
		} else {
			printType = "  Saving";
		}
		return printType + "-" + id + ", balance: " + currencyFormat.format(balance);

	}

}
