package com.revature.util;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// goal of this class is to serve as a singleton, which provides
// Connection objects to DAO classes when needed

public class ConnectionUtil {

	private static ConnectionUtil connectionUtil = new ConnectionUtil();

	private ConnectionUtil() {

	}

	public static ConnectionUtil getConnectionUtil() {
		return connectionUtil;
	}

	public Connection getConnection() {

		Properties properties = new Properties();
		try {
			// load properties file
			properties.load(new FileReader("src/main/resources/database.properties"));

			// get env variable names from properties
			String urlEnvName = properties.getProperty("url");
			String userEnvName = properties.getProperty("user");
			String passwordEnvName = properties.getProperty("password");

			// get actual environment variable data from environment
			String url = System.getenv(urlEnvName);
			String user = System.getenv(userEnvName);
			String password = System.getenv(passwordEnvName);

			// get connection using properties data
			return DriverManager.getConnection(url, user, password);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
}
