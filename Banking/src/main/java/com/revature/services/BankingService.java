package com.revature.services;

import java.util.List;

import com.revature.beans.User;
import com.revature.daos.BankingDaoImpl;
import com.revature.daos.BankingDao;
import com.revature.beans.Account;

public class BankingService {

	public BankingDao dao = new BankingDaoImpl();

	public List<Account> getAccounts(User user) {
		return dao.getAccounts(user);
	}

	public int createAccount(User user, Account account) {
		int id = dao.createAccount(user, account);
		dao.junctionTable(user, account);
		return id;
	}

	public void jointAccount(User user, Account account) {
		dao.junctionTable(user, account);
	}

	public void deleteAccount(Account account) {
		dao.deleteAccount(account);
	}

	public void setBalance(Account account, float balance) {
		dao.setBalance(account, balance);
	}

	public float getBalance(Account account) {
		return dao.getBalance(account);
	}

	public int checkCredentials(String user, String pass) {
		return dao.checkCredentials(user, pass);
	}

	public int createUser(User user) {
		return dao.createUser(user);
	}
}
