package com.revature.launcher;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import com.revature.beans.Account;
import com.revature.beans.User;
import com.revature.services.BankingService;

public class Views {
	private static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);
	private static BankingService bankingService = new BankingService();
	private static Scanner scanner = new Scanner(System.in);
	private static Account transferAccount = new Account();
	private static Account account = new Account();
	private static User jointUser = new User();
	private static User user = new User();
	private static String input = "";
	private static int attempts = 0;
	private static long elapsed;
	private static int choice;
	private static long start;
	private static long end;
	private static int hash;

	public static void main(String[] args) {
		start = System.nanoTime();
		System.out.println("Welcome to your bank of choice");
		account.setBalance(0f);
		intro();
	}
	
	/**
	 * Check to see if menu item selection is valid
	 * 
	 * @param input   - user input (int is intended)
	 * @param allowed - number of option menu selections
	 * @return		  - true/false depending on valid input
	 */
	public static boolean inputCheck(String input, int allowed) {
		end = System.nanoTime();
		elapsed = end - start;
		if ((elapsed/1000000000.0) > 30) {
			System.out.println("You have been logged off due to inactivity");
			intro();
		}
		try {
			int num = Integer.parseInt(input);
			if (num > allowed || num < 1) {
				System.out.println("Please enter a number between 1 and " + allowed);
				return false;
			}
		} catch (NumberFormatException e) {
			System.out.println("Please enter a number between 1 and " + allowed);
			return false;
		} finally {
			start = System.nanoTime();
		}
		return true;
	}

	/**
	 * Prompt inital view for user
	 */
	public static void intro() {
		System.out.println("\n1: Register as new user\n" + "2: Log in");
		input = scanner.next();

		end = start = System.nanoTime();;
		
		if (inputCheck(input, 2)) {
			choice = Integer.parseInt(input);
			if (choice == 1) {
				credentials(true, false);
			} else {
				credentials(false, false);
			}
		} else {
			intro();
		}
	}

	/**
	 * Check user credentials given username and password
	 * 
	 * @param newUser - Register user instead of log in
	 * @param joint   - Check joint account user 
	 * @return        - id for valid user or -1 or error
	 */
	public static int credentials(boolean newUser, boolean joint) {
		if (newUser) {
			System.out.println("Choose a username and password");
		} else if (joint) {
			System.out.println("You will be filling out for the joint user");
		} else {
			System.out.println("Welcome back");
		}
		System.out.println("Enter username:");
		String username = scanner.next();
		System.out.println("Enter password:");
		String password = scanner.next();
		
		hash = 0;
		String hashed = username+password;
		for (int i = 0; i < hashed.length(); i++) {
		    hash = hash*31 + hashed.charAt(i);
		}

		if (newUser) {
			user.setUsername(username);
			user.setPassword(hash+"");
			int id = bankingService.createUser(user);
			if (id == -1) {
				System.out.println("Account already exists");
				intro();
			} else {
				user.setId(id);
			}
			home();
		} else {
			int id = bankingService.checkCredentials(username, hash+"");
			if (id == -1) {
				System.out.println("Wrong Credentials\n");
				if (attempts > 2) {
					System.out.println("Too many attempts\n");
					home();
				}
				if (joint) {
					credentials(false, true);
				} else {
					attempts++;
					credentials(false, false);
				}
			} else if (joint) {
				return id;
			} else {
				user.setId(id);
				home();
			}
		}
		return -1;
	}

	/**
	 * Prompt user's home view
	 */
	public static void home() {
		System.out.println("What would you like to do?\n  1: Sign up for new account\n" + "  2: View Accounts\n" + "  3: Log out");

		input = scanner.next();

		if (inputCheck(input, 3)) {
			choice = Integer.parseInt(input);
			if (choice == 1) {
				newAccount();
			} else if (choice == 2) {
				accounts(false);
			} else {
				intro();
			}
		} else {
			home();
		}
	}

	/**
	 * Prompt view for new account setup
	 */
	public static void newAccount() {
		System.out.println("Choose your account:\n" + "  1: Checking\n" + "  2: Saving\n" + "  3: Joint Checking\n"
				+ "  4: Joint Saving\n" + "  5: Go to Home\n");

		input = scanner.next();

		if (inputCheck(input, 5)) {
			choice = Integer.parseInt(input);
			if (choice == 1) {
				account.setType("C");
				bankingService.createAccount(user, account);
				accounts(false);
			} else if (choice == 2) {
				account.setType("S");
				bankingService.createAccount(user, account);
				accounts(false);
			} else if (choice == 3) {
				account.setType("C");
				bankingService.createAccount(user, account);
				int otherUser = credentials(false, true);
				jointUser.setId(otherUser);
				bankingService.jointAccount(jointUser, account);
				accounts(false);
			} else if (choice == 4) {
				account.setType("S");
				bankingService.createAccount(user, account);
				int otherUser = credentials(false, true);
				jointUser.setId(otherUser);
				bankingService.jointAccount(jointUser, account);
				accounts(false);
			} else {
				home();
			}
		} else {
			newAccount();
		}
	}

	/**
	 * List accounts of the current user (includes joint)
	 * 
	 * @param transfer - prompts for account to transfer into
	 */
	public static void accounts(boolean transfer) {
		int num = 1;
		List<Account> accounts = bankingService.getAccounts(user);
		System.out.println("Select your account:");

		for (Account account : accounts) {
			System.out.println("  " + num + ": " + account.toString());
			num++;
		}

		if (accounts.isEmpty()) {
			System.out.println("You haven't set up any accounts yet");
		}
		System.out.println("  " + num + ": Home\n");

		input = scanner.next();

		if (inputCheck(input, accounts.size() + 1)) {
			choice = Integer.parseInt(input);
			if (choice == accounts.size() + 1) {
				home();
			} else {
				int id = accounts.get(choice - 1).getId();
				if (transfer) {
					transferAccount.setId(id);
				} else {
					account.setId(id);
					account();
				}
			}
		} else {
			accounts(false);
		}
	}

	/**
	 * Prompts account view, options available for a single account
	 */
	public static void account() {
		System.out.println("What would you like to do with your account?\n" + "  1: Deposit\n" + "  2: Withdrawl\n"
				+ "  3: Check Balance\n" + "  4: Transfer\n" + "  5: Close Account\n" + "  6: Accounts\n" + "  7: Home");

		input = scanner.next();

		if (inputCheck(input, 7)) {
			choice = Integer.parseInt(input);
			if (choice == 1) {
				System.out.println("How much would you like to deposit?\n");
				input = scanner.next();

				float deposit = 0;
				try {
					deposit = Float.parseFloat(input);
					float balance = bankingService.getBalance(account);

					if (deposit > 10000000) {
						System.out.println("Sorry, you can deposit at most $10,000,000\n");
					} else {
						bankingService.setBalance(account, balance + deposit);
					}
				} catch (Exception e) {
					System.out.println("Not a valid number\n");
				}
			} else if (choice == 2) {
				System.out.println("How much would you like to withdrawl?");
				input = scanner.next();
				float withdrawl = 0;
				try {
					withdrawl = Float.parseFloat(input);
					float balance = bankingService.getBalance(account);

					if (withdrawl > balance) {
						System.out.printf("Sorry, you only have %s \n\n", currencyFormat.format(balance));
					} else {
						bankingService.setBalance(account, balance - withdrawl);
					}
				} catch (Exception e) {
					System.out.println("Not a valid number\n");
				}
			} else if (choice == 3) {
				float balance = bankingService.getBalance(account);
				System.out.printf("Your balance is %s \n\n", currencyFormat.format(balance));
			} else if (choice == 4) {
				System.out.println("How much would you like to transfer?\n");
				input = scanner.next();
				float transfer = 0;
				try {
					transfer = Float.parseFloat(input);
					accounts(true);
					float balance = bankingService.getBalance(account);
					float balanceTransfer = bankingService.getBalance(transferAccount);

					if (transfer > balance) {
						System.out.printf("Sorry, you only have %s \n\n", currencyFormat.format(balance));
					} else if (account.getId() == transferAccount.getId()) {
						System.out.println("Transfer is unnessesary\n");
					} else {
						bankingService.setBalance(account, balance - transfer);
						bankingService.setBalance(transferAccount, balanceTransfer + transfer);
						accounts(false);
					}
				} catch (Exception e) {
					System.out.println("Not a valid number\n");
				}
			} else if (choice == 5) {
				System.out.println("Are you sure you would like to close? y/n");
				input = scanner.next();

				if (input.equals("y")) {
					bankingService.deleteAccount(account);
					System.out.println("Account closed\n");
					accounts(false);
				} else {
					System.out.println("Account not closed\n");
				}
			} else if (choice == 6) {
				accounts(false);
			} else {
				home();
			}
			account();
		} else {
			account();
		}
	}
}
