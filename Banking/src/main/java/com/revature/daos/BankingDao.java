package com.revature.daos;

import java.util.List;

import com.revature.beans.Account;
import com.revature.beans.User;

public interface BankingDao {
	public List<Account> getAccounts(User user);
	
	public int createAccount(User user, Account account);
	
	public void deleteAccount(Account account);

	public int createUser(User user);
	
	public void junctionTable(User user, Account account);
	
	public void setBalance(Account account, float balance);
	
	public float getBalance(Account account);
	
	public int checkCredentials(String user, String pass);
	
}
