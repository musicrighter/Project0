package com.revature.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.revature.beans.User;
import com.revature.beans.Account;
import com.revature.util.ConnectionUtil;

public class BankingDaoImpl implements BankingDao {

	private ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();

	private Account extractAccounts(ResultSet results) throws SQLException {
		Account account = new Account();
		account.setId(results.getInt("acc_id"));
		account.setBalance(results.getFloat("acc_bal"));
		account.setType(results.getString("acc_type"));
		return account;
	}

	@Override
	public List<Account> getAccounts(User user) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "select accounts.id as acc_id, accounts.balance as acc_bal, accounts.type as acc_type"
					+ "    from junction_accounts_users"
					+ "    join accounts on (junction_accounts_users.accounts_id = accounts.id)"
					+ "    where junction_accounts_users.users_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, user.getId());
			ResultSet results = ps.executeQuery();
			List<Account> accounts = new ArrayList<>();
			while (results.next()) {
				accounts.add(extractAccounts(results));
			}
			return accounts;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int createAccount(User user, Account account) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO accounts (balance, type) VALUES (?, ?) RETURNING id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setFloat(1, 0f);
			ps.setString(2, account.getType());
			ResultSet results = ps.executeQuery();
			if (results.next()) {
				account.setId(results.getInt("id"));
				return results.getInt("id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public void junctionTable(User user, Account account) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO junction_accounts_users (accounts_id, users_id) values (?, ?);";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, account.getId());
			ps.setInt(2, user.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteAccount(Account account) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "DELETE FROM junction_accounts_users WHERE accounts_id = ?;"
					+ "DELETE FROM accounts WHERE id = ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, account.getId());
			ps.setInt(2, account.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setBalance(Account account, float balance) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "UPDATE accounts SET balance = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setFloat(1, balance);
			ps.setInt(2, account.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int checkCredentials(String user, String pass) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT id, username, password FROM users WHERE username = ? AND password = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user);
			ps.setString(2, pass);
			ResultSet results = ps.executeQuery();

			if (results.next()) {
				return results.getInt("id");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public float getBalance(Account account) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT balance FROM accounts WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, account.getId());
			ResultSet results = ps.executeQuery();

			if (results.next()) {
				return results.getFloat("balance");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int createUser(User user) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO users (username, password) VALUES (?, ?) RETURNING id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ResultSet results = ps.executeQuery();

			if (results.next()) {
				return results.getInt("id");
			}

		} catch (SQLException e) {
			return -1;
		}
		return -1;
	}
}