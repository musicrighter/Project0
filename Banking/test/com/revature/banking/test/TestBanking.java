package com.revature.banking.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.ObjectInputStream;

import org.junit.Before;
import org.junit.Test;
import com.revature.Main;
import com.revature.Animal;
import com.revature.BlackPug;
import com.revature.YellowLab;
import com.revature.exceptions.*;


public class TestBanking {
	YellowLab firstLab = new YellowLab();
	BlackPug firstPug = new BlackPug();
	
	@Before
	public void setup() {
	}
	
	@Test(expected = NumberFormatException.class)
	public void labJumpTest() {
		assertEquals("jumping to " + -1, firstLab.jump("-1")); 
		assertEquals("jumping to " + 0, firstLab.jump("0")); 
		assertEquals("jumping to " + 5.5, firstLab.jump("5.5")); 
		assertEquals("jumping to " + 10, firstLab.jump("10")); 
		assertEquals("Not Possible!", firstLab.jump("11")); 
		assertEquals("Not Possible!", firstLab.jump("11.5")); 
		firstLab.jump("ten");
	}
	
	@Test(expected = WaterNotFound.class)
	public void labSwimTest() throws WaterNotFound {
		assertEquals("swimming", firstLab.swim(true)); 
		firstLab.swim(false); 
	}
	
	@Test
	public void labBarkTest() {
		assertEquals("Bark", firstLab.bark());
	}
	
	@Test
	public void labAliveTest() {
		assertFalse(firstLab.alive);
		assertFalse(firstLab.isAlive());
		firstLab.setAlive(true);
		firstLab.isAlive();
	}
	
	@Test
	public void labBigTest() {
		assertFalse(firstLab.big);
		assertFalse(firstLab.isBig());
		firstLab.setBig(true);
		firstLab.isBig();
	}
	
	@Test
	public void labNameTest() {
		assertEquals(null, firstLab.name);
		firstLab.setName("Zoey");
		assertEquals("Zoey", firstLab.getName());
		firstLab.setName("Dog");
		assertEquals("Dog", firstLab.getName());
	}
	
	@Test
	public void labGetGoodTest() {
		assertEquals("Amazing", Animal.good);
		assertEquals("Amazing", firstLab.getGood());
	}
	
	@Test(expected = NumberFormatException.class)
	public void pugSleepTest() {
		assertEquals("sleeping for " + -1, firstPug.sleep("-1")); 
		assertEquals("sleeping for " + 0, firstPug.sleep("0")); 
		assertEquals("sleeping for " + 5.5, firstPug.sleep("5.5")); 
		assertEquals("sleeping for " + 10, firstPug.sleep("10")); 
		assertEquals("Not Possible!", firstPug.sleep("11")); 
		assertEquals("Not Possible!", firstPug.sleep("11.5")); 
		firstLab.jump("ten");
	}
	
	@Test(expected = FoodNotFound.class)
	public void pugEatTest() throws FoodNotFound {
		assertEquals("eating", firstPug.eat(true)); 
		firstPug.eat(false); 
	}
	
	@Test
	public void pugBarkTest() {
		assertEquals("Bark", firstPug.bark());
	}
	
	@Test
	public void pugAliveTest() {
		assertFalse(firstPug.alive);
		assertFalse(firstPug.isAlive());
		firstPug.setAlive(true);
		firstPug.isAlive();
	}
	
	@Test
	public void pugBigTest() {
		assertFalse(firstPug.big);
		assertFalse(firstPug.isBig());
		firstPug.setBig(true);
		firstPug.isBig();
	}
	
	@Test
	public void pugNameTest() {
		assertEquals(null, firstPug.name);
		firstPug.setName("Buggy");
		assertEquals("Buggy", firstPug.getName());
		firstPug.setName("Doggo");
		assertEquals("Doggo", firstPug.getName());
	}
	
	@Test
	public void pugGetGoodTest() {
		assertEquals("Amazing", Animal.good);
		assertEquals("Amazing", firstPug.getGood());
	}
	
	@Test
	public void persistTest() {
		Main.persist();
		
		File pug = new File("bug.ser");
		BlackPug pugRead = new BlackPug();
		

		try (FileInputStream fis = new FileInputStream(pug)) {
			ObjectInputStream in = new ObjectInputStream(fis);
			pugRead = (BlackPug) in.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		assertEquals(pugRead.getClass(), new BlackPug().getClass());
		
	}
}
